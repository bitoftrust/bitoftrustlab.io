+++
title = "Bit of Trust Workshop - ePIC 2018"
description = "Notes by Nate Otto of the Bit of Trust Workshop in Paris during ePic 2018"
date = 2018-10-30T08:42:39Z
weight = 20
draft = false
+++


# Bit of Trust Workshop - ePIC 2018

**This is the rough transcript of a workshop we did during ePIC2018 in which we brainstormed with the Open Badges & Open Recognition community about the reasoning behind BitofTrust and the relationship of it with Open Badges & Open Recognition**

**Facilitators**: Bert Jehoul, Agis Papantoniou  
**Notes**: Nate Otto

We find that even Open Badges are difficult to implement. Many people put in the effort to design badge systems, but designers find that there is still quite a bit of effort to get started. Bit of Trust is an idea that tries to bring the concept of recognition back to its simplest and most democratic. But what does that mean in detail? Let's take some notes, define principles and discover what the opportunity for how Bit of Trust might improve on the 2018 Open Badges state of the art by radically simplifying and democratizing the concept.

"Instead of directing resources ot the elimination of trust, we should direct our resources to the creation of trust" - a reaction to blockchain. There is no problem with double spending your endorsements -- when you give one, you do not have less to give.

## Recipient Identifiers
Most Open Badges are issued to email addresses; we have many problems with people who lose access to an identifier that is essentially leased and never under their control.

Self-sovereign identity is a concept that may come into play. We explore this in Bit of Trust. In this community, we understand that when you define an identity, this can never be something held entirely by an individual, because identity is constructed in community and consists of relationships. Your identity is only you so much as that is how you are identified in the relationships 

## Bit of Trust Principles
### Everyone has the power to trust.
Everybody should have the power to give recognition, but is that truly the case so far in the Open Badges ecosystem? How much of the value of a badge in the current day has to do with the reputation of the (usually institution) that issues it? And if an endorsement comes into play, how do we really build a web of trust to understand why an endorser would be a relevant authority about a specific topic. Is a badge from an organization without a strong reputation possible to be a good badge?

### Everybody can receive recognition
Open Badges 2.0 opens the idea that issuers can also receive assertions, but this is 

### Bits of Trust should be a record of the relationship between entities, not the sole word of an issuer.
We even lose some of the true collapsing of the difference between issuer and recipient by using these terms in our diagrams for bit of trust, but the idea is that it may be a record of a relationship.

Peer claims/peer endorsements could be a type of Open Badges Assertion with an emphasis on light reusable credential descriptions (BadgeClasses) to be awarded by many different issuers. Perhaps if a Bit of Trust diverges from Open Badges it is as an Assertion that has no BadgeClass (perhaps as an Open Badges Endorsement, which follows essentially this structure).

## Building Blocks

* Open Badgess (and what we have learned)
* Decentralized Identifiers (DIDs) with cryptographic verifiability
* Simplicity: "Everything should be made as simple as possible, but no simpler."

## Use Cases and UX Challenges

### Kirkpatrick Level 3 Evaluation
Learner indicates they want to learn a skill. Trainer delivers instruction and signs off that training was received. The Employer or colleague of the learner then completes the circle by signing that the learning has been put into practice. In Open Badges this would be three badges with no clear relationship. In Bit of Trust this could be a single entity signed by each party.

```json
{
    "type": "Assertion",
    "claim": {
        "id": "did:example:youngmistwelder",
        "holdsBitOfTrust": "did:bitoftrust:tigweldexpert",
    },
    "issuer": "did:example:youngmisterwelder" // self-issued
    "proof": [
        {
            "issuer": "did:example:youngmisterwelder94", // the learner/initiator
            ...
        },
        {
            "issuer": "did:example:weldingtrainer65", // the instructor
            ...
        },
        {
            "issuer": "did:example:weldingmanager72", // the manager/validator
            ...
        },
    ]
}
```

### Application/Approval Workflow
In Open Badge Factory, they are seeing most of their users are using an application/approval workflow, which is particularly strong for recognizing prior learning. Potential recipients have the ability to submit descriptions of the evidence they have, which is then validated by the issuer of a badge.

```json
{
    "type": "Assertion",
    "claim": {
        "id": "did:example:younglearner",
        "holdsBitOfTrust": "did:bitoftrust:skill25",
    },
    "issuer": "did:example:coolteacher" // self-issued
    "evidence": [{
        "id": "https://photogallery.example.com/177932",
        "narrative": "This is a photo of my presentation poster session at #ePIC2019!"
    }]
    "proof": [
        {
            "issuer": "did:example:younglearner", // the learner/initiator
            ...
        },
        {
            "issuer": "did:example:coolteacher", // the instructor
            ...
        },
    ]
}
```

### Working with public/private keypairs
* Encrypted private key QR code that requires the decryption passphrase at usage time.

### Workflow for revocation and reissuing
* With Open Badges at present, nothing specific supports this workflow; notification to relevant users and invitation to recertify is left up to issuing platforms. Perhaps we could simplify this with Bit of Trust.
* Perhaps, like a Tamagotchi(TM), the Bit of Trust must be "fed and watered" to maintain validity -- something like a smart contract that requires further action by the recipient to keep it valid.

### How do I find a good restaurant in my town for a particular type of cuisine?
In the Open Badges ecosystem, it's very hard to reason about whether two badges from two different issuers are attempting to recognize the same thing. This could be radically simplified. Suppose somebody asks the question "What is a good Chinese restaurant in Eugene, Oregon, USA?", in the process creating a type of Bit of Trust that might be awarded and deposited into a community index. Then members of the community could add their endorsements of the relevant restaurants in the community with a small comment about it.

Example using traditional assertion structure:
```json
{
    "type": ["BitOfTrust", "Assertion"],
    "badge": "did:bitoftrust:whatisagoodchineserestaurant",
    "recipient": {
        "type": "url"
        "identity": "https://houseofawesomesteamedbuns.com"
    },
    "narrative": "The steamed buns are so good!",
    "issuer": "did:example:abc123",
    "proof": {...} 
}
```

Example using endorsement (Verifiable Credentials) structure:
```json
{
    "type": ["Assertion"],
    "claim": {
        "id": "https://houseofawesomesteamedbuns.com",
        "endorsementComment": "The steamed buns are so good!",
        "holdsBitOfTrust": "did:bitoftrust:whatisagoodchineserestaurant"
    },
    "issuer": "did:example:abc123",
    "proof": {...} 
}
```

These are both quite a bit simpler than the common Open Badges Assertions that exist today. With the first, we would need to implement 

### Abuse Prevention and Remediation
How can we limit the ability to cause harm by doxxing, by using open linked data technologies such as Bit of Trust? When bad actors enter communities and issue bits of trust that are abusive and against the standards of a community, how can that community respond to maintain a positive vibe and prevent harm to its members?

### Understanding the Trust Graph
* How can Bit of Trust be resistant to "buying followers?" How do we build an authentic trust network that is able to be analyzed at a deeper level than "how many follows does this entity have?"
* How can we incorporate a "currency" metaphor? What is the exchange rate for the same bit of trust issued by different issuers?

## Recognizing Soft Skills
The assessment of soft skills often fails when formalized. Who cares about the formal assessment of a soft skill? 

### Informal Recognition
We hope to make Bit of Trust available and very simple for implementation in informal use cases. What must be done?

* How do we define what matters in a community so that those things can be easily recognized? 
* How can we help badge recipients participate in the process of badging as more than passive recipients?
* How can the value of a Bit of Trust be established without brand reputation?

## Open Questions about Open Badges:
### What are the main drivers of the spread of Open Badges? Why YES to badges in 2018?
### What are the main drivers that are holding back the spread of Open Badges? Why NO to badges?
### Is the recipient in control of the badges they receive? Should they be?
### What are drivers for why the badges you issue get adopted and actually used by recipients and consumers?

## Open Questions - Formal & Informal Recognition
### What communities are you part of?
### When is a community a community? (and not an organization/company/nnetwork)
### Who has access (and how) to the communities that you are in?
### How is trust established within the community?
### For an example community (e.g. your neighborhood), How are skills recognised in this community?
### For that community, How is this different from skills recognition when someone new enters your neighborhood?)

## Conclusions
Bit of Trust may become a tool for Open Recognition to facilitate the emergence of communities with internal informal recognition in relation with other communities (local trust networks) and the rest of the world (web of trust).