+++
title = "It’s about Trust, Stupid! (0/3)"
description = "Why Blockchain-based BlockCerts are the wrong solution to a false problem - by Serge Ravet"
date = 2019-01-29T08:42:39Z
weight = 20
draft = false
author = 'Serge Ravet'
+++

# It’s about Trust, Stupid! Why Blockchain-based BlockCerts are the wrong solution to a false problem (0/3)

## Prolegomenon

Blockchains have become both a kind of a fashion item and a religion. Not having one on display can be considered as a sign of bad taste, ignorance, misplaced skepticism, Luddism or heathendom.

![blockchained](https://gitlab.com/bitoftrust/bitoftrust.gitlab.io/raw/master/static/img/blog/hand-749676_1920-1024x576.jpg "blockchained")

This series of posts (3) is a rebuttal of the blockchain bandwagon on to which so many are willing to
jump without engaging their brains: remember Farmingdale 
([link](https://www.cnbc.com/2017/12/21/long-island-iced-tea-micro-cap-adds-blockchain-to-name-and-stock-soars.html)), 
a $24 million iced tea company had its stock jump 200% when it declared moving into the blockchain business?
How engaged were the brains of those who decided to invest in the 1,000+ cryptocurrencies ([link](https://ethereumworldnews.com/more-than-1000-cryptocurrency-projects-have-failed-in-2018/)) that failed in 2018? And if 92% of blockchain projects have failed 
([link](https://bitcoinist.com/92-blockchain-projects-already-failed-average-lifespan-1-22-years/)) does it mean that the other 8% have succeeded? Or that they are on the soon-to fail waiting list?  

A recent call for tenders by the European Commission for the [Study on Blockchains: Legal, Governance and Interoperability Aspects](https://ted.europa.eu/TED/notice/udl?uri=TED:NOTICE:533841-2018:TEXT:EN:HTML) specified:

> “The study will enquire for legal and regulatory aspects related to blockchain-inspired technologies and their applications as well as for socio-economic impacts of the Blockchain technology. […] The study should reinforce or complement the work of the EU Blockchain Observatory and Forum, while providing useful and meaningful inputs for the deployment of a EU Blockchain Infrastructure in 2019.  

So, now it’s a given, there will be a “deployment of an EU Blockchain Infrastructure in 2019” and therefore we just have to study “legal and regulatory aspects” to “reinforce” the work of the [EU Blockchain Observatory and Forum](https://www.eublockchainforum.eu/), an “observatory” who’s primary mission is not to “observe” but :

> “to promote blockchain in Europe by mapping existing blockchain initiatives, analysing and reporting on important blockchain themes, promoting blockchain education and knowledge sharing and holding events to promote debate and discussion.”

The general contractor of the so called “observatory” is ConsenSys AG, a blockchain technology provider… It’s a bit like if Shell had been contracted by the European Commission to lead an observatory on global warming… Or the Vatican an observatory on paedophilia… It’s true that each of them have first hand experience with the subject matter.  

*Ite missa est (1)* the Blockchain deacon says. To which we are expected to respond: *Blockchain gratias*. 
That is for the catholic (which means universal) interpretation of the Blockspell, the public Blockchain worshiping the Power of Work—and global warming! 
There is also the schismatic branch worshiping the *Power of Stake*, i.e. the power of the wealthy, the *plutocrats*,
and the many sects with their own interpretation of the Blockspell which main message is: *“distrust each other, the way I distrusted you”* Satoshi 15:12.  

The pollution of discourses with blockchains masks a much more sinister and worrying reality: a form of acceptance that we will have to live in a society based on 
distrust and therefore, the need for a technology that could thrive on it. And what better technology than public blockchains for that purpose? 
Instead of developing technologies that could contribute to rebuilding trust, a *Web of Trust*, we are invited to compete in developing the technologies that do not just ignore trust, but for which trust is treated as a liability, a mortal sin: public blockchains are based on fierce competition between the so-called miners; any form of cooperation would destroy the very foundations of public blockchains. 
**Trust is the mortal enemy of public blockchains**. Public blockchains have achieved what in their wildest wet dreams the founders of De Beers couldn’t have imagined.  

If there is a growing problem with trust, or rather the lack of it, and who could deny it, there is no alternative to address it but… by rebuilding trust. 
A world without trust might be possible, but is it the one we want to live in? Rebuilding trust needs technologies that capture, elicit and activate trust,
not technologies based on the assumption that *trust is a liability*. 
A technology building trust should be… about trust. *It’s about Trust, Stupid!*  

Does it mean that we should ban blockchains from our set of tools? Certainly not! Certain types of blockchains could be valuable solutions to certain types of problems (c.f. next post). But it is quite exhausting to hear the born-again-Blockchain-believers for whom, like the person holding a hammer, everything looks like a nail. The blockchain is sometimes presented as the new panacea needed to heal the wrongs of the world. It is not just superficial, it is plain wrong: some applications of blockchain technologies can make things worse than they were, like the Bitcoin and other cryptocurrencies that are not just boosting traditional criminal activities but enabling new ones, not to mention global warming.   

In the following posts, we will explore through a concrete example, how the quest for blockchain applications can lead to the fabrication of a false problem based on a backward-looking vision of the reality. This leads to the paradox of the exploitation of a pretend “new technology” to enforce visions and values of the past, not to mention the deleterious effects on innovation, trust and the empowerment of individuals and communities.

### Notes

1- “Go it is the dismissal” generally improperly translated as “go, the mass is over.”