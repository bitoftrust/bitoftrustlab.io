+++
title = "It’s about Trust, Stupid! (1/3)"
description = "Why Blockchain-based BlockCerts are the wrong solution to a false problem - by Serge Ravet"
date = 2019-02-04T08:42:39Z
weight = 20
draft = false
author = 'Serge Ravet'
+++

# It’s about Trust, Stupid! Why Blockchain-based BlockCerts are the wrong solution to a false problem (1 of 3)

![wheel](https://gitlab.com/bitoftrust/bitoftrust.gitlab.io/raw/master/static/img/blog/wheel.png "Metaphor for blockchain-centred systems—when a part becomes the whole")

***Metaphor for blockchain-centred systems—when a part becomes the whole***

## Why Blockchains?

**The rationale for the initial development of blockchain technologies like Bitcoins, was to solve the problem of double spending while simultaneously:**

- Getting rid of regulatory bodies — the dream of the proponents of *anarcho-capitalism* also called *libertarian anarchy*, one of the ideologies widely shared between the alt-right, Trump and Silicon Valley (c.f. their track-record in tax dodging).

- Getting rid of the need for *trusted authorities* to secure transactions — which resulted in creating an ecosystem that works best when everybody is at war with everybody. *Trust* is a mortal sin as trust between the *miners* could lead to collusion and cheating.

> “Cryptocurrencies are among the largest unregulated markets in the world. We find that approximately one-quarter of bitcoin users are involved in illegal activity. We estimate that around $76 billion of illegal activity per year involves bitcoin (46% of bitcoin transactions), which is close to the scale of the US and European markets for illegal drugs.” – Foley, Karlsen, Putniņš, [Sex, Drugs, and Bitcoin: How Much Illegal Activity Is Financed Through Cryptocurrencies?](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3102645)

**This created the conditions for:**

- Bitcoin becoming the cybercash of choice for criminals and crooks— who are a common species of *libertarian anarchists* (that is… until they grab power!);
- Bitcoin becoming the cybercash of choice for speculators—another species of parasites;
- Bitcoin becoming the playground shared by “innovators” oblivious (or not) of the favour they do to money launderers, murderers for hire and the likes.

**The blockchain technology might be clever, but it does very stupid things:**

- Public blockchains based on Proof of Work (PoW) are actively contributing to global warming—Bitcoin operations consume the annual energy of New Zealand, and growing!
- Public blockchains are not just ledgers, they are *stupid ledgers* — they are unable to do what “standard” ledgers have done for millenia: differentiate between the active ledger, where one writes the current year’s transactions, and the archives from previous years that you don’t need (1), except for a possible audit. Public blockchains are literally *collectors of garbage*, a garbage that is actively sucking the energy from the planet.

**The discourse on blockchains is dominated by “concepts” that in reality mask a trivial reality or pure fantasies (and nightmares):**

- “Smart contracts” are nothing more than a hyped name for “[stupid] code”
- Public blockchains are not “trustless” but based on distrust (c.f. above)
- Decentralised Autonomous Organisation (DAO), the dystopian vision of people interacting with each other according to a protocol specified in code and enforced on the blockchain!

The description of the DAO in the article “[What is a DAO](https://blockchainhub.net/dao-decentralized-autonomous-organization/)” is what one would expect of a dysfunctional organisation lead by paranoia, distrust and a psycho-rigid sociopathic management! Here is how this “revolutionary concept” (LOL) is presented:

> “Instead of a hierarchical structure managed by a set of humans interacting in person and controlling property via the legal system, a decentralized organization involves a set of people interacting with each other according to a protocol specified in code and enforced on the blockchain.” [“no employment contracts!” indicates the accompanying graphic, *exclamation mark included!!!*]

If humans have to act according to what has been enshrined into lines of code, then aren’t we expected to behave like brainless robots? One could add *extremely stupid robots* like the dumb terminals of the early ages of computers. In fact, humans are not even being reduced to robots: the DAO is the robot, and humans are just its sensors and actuators. The underpinning dystopian vision of DAOs would make Charlie Chaplin’s *Modern Times* and Fritz Lang‘s *Metropolis* pass for Utopia!

**The underpinning discourse on blockchains is:**

- Don’t trust people—they are a liability therefore avoid building anything based on their independent judgement, only trust technology and mathematics… which leads to
- Don’t trust organisations, transform them into automata dictating to people how to behave through lines of code. Code is law (LOL)!

## Bitcoin-based credentials: an unwitting support of criminal activities?

The question of whether Bitcoin and cryptocurrency publicists are acting consciously or not as agents of criminals is a bit like asking whether Trump and his goons are *wittingly* acting as agents of Russia… At this stage of the Mueller investigation, the only thing one can say is: it *might* be unwittingly. And when a university uses and promotes Bitcoin-based authentication as a means to make its credentials more *secure*, it *unwittingly* contributes to legitimise one of the criminals’ currencies of choice.

How could a university be certain that the Bitcoins they bought to put (the hash of) their credentials on the Bitcoin Blockchain are 100% “clean”? Do they ask for a certificate of provenance attesting that they do not contain any trace of crypto-money once used in a criminal activity? Of course not, as one of the “beauties” of Bitcoins is that they come with their own simple and effective way to launder criminal gains: “tumblers”:

> “Tumblers are conceptually quite simple. You send your dirty bitcoins to a tumbler’s address along with a brand new “clean” address of your own, the tumbler mixes in bitcoin from lots of different wallets (some clean, some dirty) into the tumbler’s own wallet, and finally the tumbler has that wallet pay out to the “clean” wallet you provided (after extracting a service fee, of course).” -Tyler Elliot Bettilyon, [Cryptocurrency’s Criminal Revolution](https://medium.com/s/story/cryptocurrencys-criminal-revolution-6dae3cdf630f)

If the report outlining that 46% of Bitcoin transactions are the result of criminal activities is only half accurate, it is not unlikely that the Bitcoins bought to store (the hash of) credentials come from dirty sources. Even if the sums involved in the process are infinitesimal, and if the credentials have any value, storing (the hash of) a credential on the Bitcoin blockchain would be a bit like scattering the ashes of a beloved one on a landfill managed by the Camorra—an organisation specialised in “self-governed, distributed landfill operations!”

![landfill](https://gitlab.com/bitoftrust/bitoftrust.gitlab.io/raw/master/static/img/blog/landfill.png "landfill managed by the Camorra")

***Storing (the hash of) diplomas on the Bitcoin blockchain is a bit like scattering the ashes of the beloved ones on a landfill managed by the Camorra***

Let’s imagine for a second that governments, i.e. the people representing us, decided to declare Bitcoins transactions illegal and offered to those who used Bitcoins for a legitimate purpose (we shouldn’t consider speculation as legitimate) to convert their Bitcoins into official currencies —at a value to be established by national treasuries. Criminals and speculators would have a hard time to recover their assets, and loose a key incentive in using Bitcoins. For them, it is critical that non-criminals are actively involved in the Bitcoin ecosystem: it is a means to provide legitimacy to a channel they use to support their operations and cash in the fruit of their criminal activities.  

The point here is not to claim that, when something can be used to facilitate criminal activities, it should be banned, as that would apply, not just to luxury cars, speed boats or real estate, but also knives, public transport, email, cryptography, official currencies and the very air we breath, to name a few. Bitcoins (and a number of other cryptocurrencies) have a unique selling point: a series of features that have a transformative and multiplier effect on criminal operations.  

Those who use and promote Bitcoins are nolens volens complicit in the hiding in plain sight of criminals activities. It is a moral responsibility they should not be allowed to escape any longer.  

## How to resist blockchain mania?

Getting funding and clients is a permanent challenge and jumping on the blockchain bandwagon could sometimes be a prerequisite to be listened to by funding bodies and clients altogether. In the current strands for European funding, knowing that the European Union has decided to establish a “European blockchain infrastructure” in 2019, who will dare not to mention blockchains in their responses to the calls for tenders? And if you are a business and a client asks “when will you have a blockchain solution” what is the response most likely to get her attention: *that’s not relevant to your problem* or *we have a blockchain solution that just matches your needs*? How to resist the blockchain mania while providing clients and investors with something that sounds like what they want to hear?

One of the “beauties” of Blockchains you might want to exploit to your advantage is that you can claim almost anything in favour of blockchains with very little chance of being challenged. And if the challenges come from a heathen (someone who doesn’t believe in the Blockspell) it is most unlikely that they will sift through the walls of the blockchain echochamber. Blockchains offer many opportunities to say *n’importe quoi* to B.S. your way to [temporary] success. What is important is not *what* you write or say, but to say or write something that is consistent with the expectations of your clients and funding bodies. Don’t lose sight that most of your interlocutors will be primarily looking for a confirmation of their believes: *in blockchain we [dis]trust*.

For example, you could practise *l’art de la litote*. My attention was recently attracted to an article describing blockchains as “distributed trust” which they are not, but makes a nice and closer to the truth acronym: dis-trust… Another article describes blockchains as “minimising trust” which is yet another cute litote. If you are not an expert in l’art de la litote, or find it too subtle for your taste, you could decide to take a “concept” like DAO and imagine some wild scenario in line with the expectations of your audience. In the next post you will discover an article published by Springer presenting smart contracts as a means to “verify the consistency of the teaching design and practice” (LOL). The authors who obviously don’t have even the most elementary clue of what learning and teaching are about, certainly know how to B.S. their way to a prestigious publisher…

Going back to the fundamentals, Blockchains are a special species of distributed databases—and there are many sub-species that have emerged to fit different ecosystems. So, as almost all projects need some form of a database, you can always select an innocuous element of your design using some kind of a blockchain and make it look more or less prominent, according to the degree of gullibility of your audience and the requirements of the tender. Don’t worry if your design looks as ridiculous as the photo of the monowheel on the front-page: as long as there is a reference to blockchains and you use the right keywords—distributed, ledger, immutability (LOL), consensus, etc.—it is likely that it will be received with utmost respect. Once you have the money and delivered a functional product without a blockchain, or used it as a non-essential element, you should be safe—unless your client or sponsor is a *blockhead*.

## Projects using blockchains don’t have to be “*blockchain projects*"

A project using blockchains is not necessarily a “blockchain project” nor a project to build a vehicle a “tire project” or a “fuel project.” One could of course design a vehicle that is entirely imagined from one of its parts, like the monowheel in the picture on the front-page. And that’s how many blockchain projects are currently designed, transforming Sullivan’s famous axiom “form follows function” into “everything should look like a blockchain.”  

It is time for a copernican revolution, moving Blockchains from the centre of all designs to its periphery, as an accessory worth exploiting, or not. If there is a need for a database, the database doesn’t have to be distributed, if there are decisions to be made, they do not have to be left to an inflexible algorithm. On the other hand, if the design requires computer synchronisation, then blockchains might be *one of the possible solutions*, though not the only one.  

The Mattereum project ([link to description](https://medium.com/humanizing-the-singularity/the-future-of-the-blockchain-my-devcon-iv-talk-f78b4fb9b95c) led by Vinay Gupta one of the founders of Ethereum, might be an early sign of a new understanding of the place of blockchains. The project is about how to control assets with code so the contracts attached to them become legally binding or at least legally disputable. The ecosystem comprises “custodians” which own an asset as a proxy for some other actor in the system and execute the [smart] contracts / run the code

Disputes are settled through *international arbitrators*: “Mattereum provides an international panel of arbitrators who are technically competent to interpret the facts of smart contract and blockchain disputes. […] Arbitrators must be assessed as competent before being accepted onto the Mattereum list.” ([link](https://mattereum.com/arbitrators/)).

Well, so much for the “self-governed, decentralised” blockchain and the “Decentralised Autonomous Organisation!” To make the whole thing work, humans have been reintroduced in the guise of highly paid lawyers—the average cost of an international arbitration is around 2 million Euros(2)… Which means that the [smart] contracts will have to also be drafted by highly paid lawyers and programmers, who in turn will probably have to pay premium insurances, in case of a bug… If it were a physical chain, it would not be made of ordinary platinum but Californium which costs USD 27 million per gram…

Vinay Gupta writes:

> “my contention is that global trade is either going to be completely centralised, or **you’re going to have to synchronise computers internationally to conduct global trade**, and for that it’s either going to be a blockchain or something equivalent.” (ibid., my highlights)

If the blockchain is freed from the ideological B.S., the *Bollockschain*, and is presented as a means to synchronise computers, it’s already a significant improvement in the right direction. And of course, blockchains are a great way to synchronise computers, but as Vinay Gupta aptly writes, it could be “something equivalent.” And there are many ways to synchronise computers that work pretty well (think of DNS, which is slow but efficient and resilient3). What Mattereum shows is that a system based on “pure distribution” with no central authority, no external regulation is a fallacy (or a criminals’ delight): in the Mattereum case, there is a central authority which is the international arbitrators, as is Mattereum who will pick and choose the arbitrators, and may be also the lawyers and programmers who will write the [not so smart] contracts.

Trust has been reintroduced. It had to be, as it’s trust that makes business possible. *It’s about trust, stupid!*


### Notes

1. Nineveh was destroyed in 612 BC […]. It is believed that during the burning of the palace, a great fire must have ravaged the library, causing the clay cuneiform tablets to become partially baked.[10] This potentially destructive event helped preserve the tablets.” source: [Library of Ashurbanipal](https://en.wikipedia.org/wiki/Library_of_Ashurbanipal). The Romans used beeswax tablets as they were easy to erase… So long the [The Blockchain Immutability Myth](https://www.multichain.com/blog/2017/05/blockchain-immutability-myth/)!: “In the raucous arena of blockchain debate, immutability has become a quasi-religious doctrine – a core belief that must not be shaken or questioned.”  
2. “The CIArb Costs of International Arbitration Survey, based on 254 arbitrations conducted between 1991 and 2010, contains the most extensive analysis of the cost of international arbitration to date. […] the overall average cost of international arbitration to be approximately GBP 1,580,000 (USD 2.6 million or EUR 2 million using recent exchange rates) for Claimants, and approximately 10% less for Respondents”. ([source](https://www.international-arbitration-attorney.com/cost-of-international-arbitration/))
3. The problem that blockchain solves here, is that when you have a financial trading transaction, there will always be a time delay to let that transaction travel over all servers in the world . What blockchain does is dividing time into blocks before validating a transaction through the distributed consensus. 

