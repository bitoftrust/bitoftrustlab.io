+++
draft= false
title = "FAQ"
description = "Asked and answered"
+++

## How can I contribute?

First of all thank you for considering contributing to the home of Bit of Trust development.  
We welcome any contribution and encourage you to do so if you have anything to correct, update or add to this site.  
Contributing is as easy as just [adding a markdown file into a folder in our Gitlab repository](https://gitlab.com/bitoftrust/bitoftrust.gitlab.io/tree/develop/content).  
More info on how to contribute you can find [here](https://bitoftrust.gitlab.io/docs/how-to-contribute-to-this-bitoftrust-infosite/).

## Who is behind BitofTrust?

**Bit of Trust** is a STARTUP founded in 2018 on the initiative of [internationally recognized experts](https://bitoftrust.gitlab.io/team/) in the field of development, recognition and valorization of skills.  
The mission of Bit of Trust is to design, develop and support the implementation of technologies and services that contribute to the construction of a Web of Trust, serving an open society.  
Its main areas of intervention are education, employment, human resources, territorial development and social integration.  
Bit of trust is one of the founding member of Reconnaître – **Open Recognition Alliance**

You can find more info on the people involved: [here](https://bitoftrust.gitlab.io/team/).