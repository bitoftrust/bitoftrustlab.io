+++
title = "How to Contribute to Bit of Trust Homepage"
description = "Guide how to add blogposts & documentation markdown files to Bit of Trust homepage on Gitlab"
date = 2018-09-19T08:37:21Z
weight = 20
draft = false
bref = ""
toc = true
+++

# How to Contribute

First of all thank you for considering contributing to the home of Bit of Trust development.  
We welcome you to the community and are very happy & thankfull you want to help out.  

### What to contribute ? 

- add a [blogpost](https://bitoftrust.gitlab.io/blog/)
- add or improve [documentation](https://bitoftrust.gitlab.io/docs/)
- add or answer a question to our [FAQ](https://bitoftrust.gitlab.io/faq/)
- improve the general design or look & feel of our pages
- correct spelling ~~mitstakes~~ mistakes
- improve any content
- ...

### How to contribute documentation & blogposts ?

This site is build with [Hugo](https://gohugo.io/) and adding content is as simple as **adding a Markdown file** in the correct folder.  
So the easiest way to contribute is to just add your content by adding a markdown file
[on Gitlab directly in one of the content folders in the development branch](https://gitlab.com/bitoftrust/bitoftrust.gitlab.io/tree/develop/content).  
We will check these and put them live in our next deployment. 

But for those who would like to follow the long way or who want to do more than just adding a markdown file, these are the more detailed guidelines & gitlab workflow we like to follow:

### Comprehensive Contributing Guidelines

##### Issue reporting

- You can find all issues [here](https://gitlab.com/bitoftrust/bitoftrust.gitlab.io/issues) 
- Before you file an issue, make sure it doesn't yet exist
- Be as **clear** as possible in your title and description of the issue
- As with branches, we like our issue titles to be lowercase and seperated by dashes e.g **add-this-feature**
- Make sure your issue only tackles **one problem at a time**
- Try to **label** your issue appropriate & distinguish at least between feature, bug, blogpost or documentation
- Try to assign them to a **milestone** if necessary or possible
- Only use the labels of the [issue board](https://gitlab.com/bitoftrust/bitoftrust.gitlab.io/boards?=) : *To Do, In Review, Doing, Blocked* , if the issue is being worked on or has been selected to get worked on
- You may create new labels if you like, maintainers of the repo will do clean-ups when necessary

##### Merge Requests

- The `develop` branch is the default branch to checkout from and to merge back against.
- The `master` branch is a protected, production branch and reflects the version that is live on [https://bitoftrust.gitlab.io/](https://bitoftrust.gitlab.io/)

##### Want to **start working on an issue** ? 

- Our preferred workflow is the default one of Gitlab, available through the Gitlab UI via the button **Create merge request** on the page of an issue:
        
    - make sure to always start from an issue, please file one first if it doesn't exist yet
    - Branch from `develop`
    - name your branch starting with the number of the issue you are working on & the name of the issue e.g. `15-add-feature-y`
    - we like our branches (as our issues) to be lowercase and seperated by dashes
    - you may push as many commits as needed in your branch (you can squash them before merge)
    - work in the src folder and don't push the `dist` in the commits (.gitignore leaves it out anyway)
    - by Gitlab convention: we like you to already file a merge request during the time you are working on it, but make sure to add **WIP** before your title so it will not be accidently merged: e.g.`WIP: Resolve "name-of-the-issue"`
    - Always merge back against the `develop` branch
    - Best practice is to request a review before you actually merge, so someone else can adjust/approve/comment, label the issue also 'In review'
    - When approved you may merge to the `develop` branch
    - **clean up** after you merge by removing the source branch that you merged from & **closing the issue** (which Gitlab should do automatically if you followed guidelines)

### Deployment pipeline

- Via .gitlab-ci.yml a CI/CD pipeline is triggered when anything is merged into the `master` branch , this will automatically build & deploy the app to [https://bitoftrust.gitlab.io](https://bitoftrust.gitlab.io).
- Only maintainers can merge into the master branch

### Development Setup

This site is made with [Hugo](https://gohugo.io/).  
To run, build & develop it locally you need to [have hugo installed](https://gohugo.io/getting-started/installing/)
`